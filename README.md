# kubernetes-2024
# Prerequisites
- [ ] Virtualbox
- [ ] Vagrant
- [ ] [Click Here to install vscode](https://code.visualstudio.com/)
- [ ] [Click Here to install git bash](https://git-scm.com/downloads)



## Getting started
- [Container Basics](https://gitlab.com/container-and-kubernetes/kubernetes-2024/-/issues/6)
- [Day 1](https://gitlab.com/container-and-kubernetes/kubernetes-2024/-/issues/1)
- [Day 2](https://gitlab.com/container-and-kubernetes/kubernetes-2024/-/issues/2)
- [Day 3](https://gitlab.com/container-and-kubernetes/kubernetes-2024/-/issues/3)
- [Day 4](https://gitlab.com/container-and-kubernetes/kubernetes-2024/-/issues/4)
- [Day 5](https://gitlab.com/container-and-kubernetes/kubernetes-2024/-/issues/5)
